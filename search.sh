url=$(cat $HOME/.config/surf/bookmarks.txt | dmenu -p "Site:")
if [$url = ""]; then
    exit
fi
surf -gs $url
